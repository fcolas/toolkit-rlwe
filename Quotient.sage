# -*- coding: utf-8 -*-

#
# xgcd of a list a
#
def xgcd2(a):
    s = len(a)
    c = ZZ(int(random()*10^s+1))
    L = matrix(ZZ, s+1, s, lambda i, j: kronecker_delta(i, j))
    L.set_row(s, a)
    L.rescale_row(s, c)
    U = (L.transpose()).LLL()
    u = list(U[s-1])[:-1]

    return (sum([ai*ui for (ai, ui) in zip(a, u)]), u)


#
# Find q with parameter m and p
#
def find_q(m, p, ss=1):
    mhat = m/2 if(m%2 == 0) else m
    l = 2
    s = p*sqrt(ss^2 + 2*pi*radical(m)/m)
    q = next_prime(int(s*sqrt(2*(mhat*l)^2 + euler_phi(m))))
    
    while(Mod(q, m) != 1 and Mod(q, p) == 1):
        q = next_prime(q)

    return q


#
# Iterator for zeta_m = omega^i with i in (Z_m)*
#
def zeta(m):
    omega = CC(e^(2*I*pi/m))
    for i in xrange(m):
        if(gcd(i, m) == 1):
            yield omega^i

    # return (omega^i for i in xrange(m) if gcd(i, m) == 1)


#
# Iterator with morphism a -> sigma_i(a)
#
def sigma_i(m, K):
    F = Hom(K, CC)
    for z in zeta(m):
        yield F([z], check=False)

    # return (F([z], check=False) for z in zeta(m))


#
# Return a tuple with sigma_i(a) for i in (Z_m)*
#
def sigma(a, K):
    F = Hom(K, CC)
    return vector([s(K(lift(a))) for s in sigma_i(m, K)])


#
# Trace of an element a in K
#
def Tr(a, K):
    # equivalent to reduce(lambda x, y: CC(x)+CC(y), sigma(a))
    return sum(sigma(a, K))


#
#  Norm of an element a in K
#
def N(a, K):
    return prod(sigma(a, K))


#
# The absolute determinant of m
#
def absdet(m):
    return (m/prod([p^(1/(p-1)) for p in prime_factors(m)]))^euler_phi(m)


#
# Discrete Fourier Transform matrix
#
def DFT(m):
    omega = CC(e^(2*I*pi/m))
    return matrix(CC, m, m, lambda i, j: omega^(i*j))


#def DFT(m):
#    return matrix(K, m, m, lambda i, j: f(r)^(i*j))


#
# Chinese Remainder Transform matrix
#
def CRT(m):
    D = DFT(m)
    C = D.matrix_from_rows_and_columns([i for i in xrange(m) 
                                        if gcd(i+1, m) == 1], 
                                       [j for j in xrange(euler_phi(m))])
    return C


#
# Return (Z_m)* as a list
#
def coprime_list(m):
    return [k for k in xrange(m) if gcd(k, m) == 1]


#
# Useful to flatten tensor_product lists
#
def to_list(l):
    return flatten(map(flatten, list(l)))


#
# Return p the powerful basis of K
#
def PowerfulBasis(m, K):
    mi = [a^b for (a, b) in factor(m)]
    l = [vector([ki^j for j in xrange(euler_phi(mj))]) 
         for (ki, mj) in zip(K.gens(), mi)]

    return vector(reduce(lambda x, y: 
                         to_list(vector(x).outer_product(vector(y))), l))


#
# Return b the power basis of K
#
def PowerBasis(m, K):
    g = K.gen()
    return vector([g^i for i in range(euler_phi(m))])


#
# Return d the decoding basis of K
#
def DecodingBasis(m, K):
    mi = [a^b for (a, b) in factor(m)]
    l = [vector([ki^(-j) for j in xrange(euler_phi(mj))]) 
         for (ki, mj) in zip(K.gens(), mi)]

    return vector(reduce(lambda x, y: 
                         to_list(vector(x).outer_product(vector(y))), l))


#
# "Basis" of a polynomial P
#
def PolyBasis(P):
    base = P.monomials()
    base.reverse()
    
    return (vector(lift(P).coefficients()), vector(base))


#
# Vector of a polynomial P in basis B
#
def pol_in_B(P, B):
    P = lift(P)
    p = []
    for (i, b) in enumerate(B):
        if b in P.monomials():
            p.append(P.monomial_coefficient(lift(b)))
        else:
            p.append(0)

    return vector(p)


#
# Polynomial of a vector vec in basis B
#
def vecB_in_P(vec, B, K):
    P = K(0)
    for (i, v) in enumerate(vec):
        if(v != 0):
            P = P + v*B[i]

    return P


#
# Round a vector v
#
def Round(v):
    return vector([vi.round() for vi in v])


#
# Sample n vector with continuous Gaussian of parameter sigma
#
def sample_psi(n, sigma=1):
    Psi = RealDistribution('gaussian', sigma)

    return vector([Psi.get_random_element() for _ in xrange(n)])


#
# Sample P in Rdual = <t^-1>
#
def sample_dual(Rdual):
    return f(K.random_element())*Rdual


#
# Discretisation
#
def discretize(x, c, n, B):
    if(c in ZZ):
        c = vector(QQ, n)
        cc = x-c
    else:
        cc = x-c
    #cc = Round(cc)
    y = []

    for (ci, bi) in zip(cc, B):
        if(round(random())):
           y.append((1-ai)*bi)
        y.append(ai*bi)

    return vector(y)


#
# Keygen
#
def keygen(m, n, p, B, Rq):
    mhat = m/2 if(m%2 == 0) else m
    a = Rq.random_element()
    s = discretize(sample_psi(n), 0, n, B)
    e = discretize(p*sample_psi(n), 0, n, B)
    b = Rq(a*s+e)
    b = b*mhat

    return (a, b)


#
# Encryption
#
def enc(pk, m, mu, q, B, Rq):
    (a, b) = (pk[0], pk[1])
    mhat = m/2 if(m%2 == 0) else m
    z = discretize(sample_psi(n), 0, n, B)
    ee = discretize(p*sample_psi(n), 0, n, B)
    c = vector(GF(q), list(tinv*mu))
    eee = discretize(p*sample_psi(n), c, n, B)
    u = Rq(mhat*(z*a+ee))
    v = z*b + eee

    return (u, v)


#
# Decryption
#
def dec(sk, c, m, B, Rp, Kt):
    (u, v) = (sk[0], sk[1])
    mhat = m/2 if(m%2 == 0) else m
    d = decode(v - u*s)
    g = prod([(1-ki) for ki in Kt.gens()])
    t = mhat / g

    return Rp(t*d)


    

# Parameters for RLWE instance
m = 3*5*7
n = euler_phi(m)
q = 2311
p = 2
sg = p*sqrt(1 + 2*pi*radical(m)/m)


# Decomposition of m
mi = [a^b for (a, b) in factor(m)]
mihat = [ZZ(m/mj) for mj in mi]
ei = xgcd2(mihat)[1]
mhat = m/2 if(m%2 == 0) else m


# mth cyclotomic polynomial
Phi_m = cyclotomic_polynomial(m)


# Rings with Z-basis
Z.<z> = PolynomialRing(ZZ)
ZqZ = IntegerModRing(q)
Zq.<zq> = PolynomialRing(ZqZ)
R.<r> = Z.quo(Phi_m)
Rq.<rq> = Zq.quo(Phi_m)

Zl = PolynomialRing(ZZ, len(mi), 'z', order='lex')
IdlZ = [cyclotomic_polynomial(mj, 'z'+str(i)) 
        for (i, mj) in enumerate(mi)]
Rt = Zl.quo(IdlZ, 'r')
Zi = [ZZ[zi].quo(Idli, 'zi'+str(i)) 
      for (i, (zi, Idli)) in enumerate(zip(Zl.gens(), IdlZ))]
# Zt = Kl.quo(IdlZ, 'z') # not yet!


# Fields with Q-basis
Q.<x> = PolynomialRing(QQ)
K.<k> = Q.quo(Phi_m)

Kl = PolynomialRing(QQ, len(mi), 'q', order='lex')
IdlQ = [cyclotomic_polynomial(mj, 'q'+str(i)) 
        for (i, mj) in enumerate(mi)]
Qi = [QQ[qi].quo(Idli, 'qi'+str(i)) 
      for (i, (qi, Idli)) in enumerate(zip(Zl.gens(), IdlQ))]
Kt = Kl.quo(IdlQ, 'k') 


"""
# Fields with R-basis
Real.<y> = PolynomialRing(RR)
KR.<kr> = Real.quo(Phi_m)

KRl = PolynomialRing(RR, len(mi), 'y', order='lex')
IdlR = [cyclotomic_polynomial(mj, 'y'+str(i)) 
        for (i, mj) in enumerate(mi)]
Ri = [RR[yi].quo(Idli, 'yi'+str(i)) 
      for (i, (yi, Idli)) in enumerate(zip(KRl.gens(), IdlR))]
KRt = KRl.quo(IdlR, 'kr') 
"""


# Construction of morphism f: K -> Kt
P = prod([ki^ej for (ej, ki) in zip(ei, Kt.gens())])
F = Hom(K, Kt)
f = F([P])


"""
# Construction of morphism f: KR -> KRt
P2 = prod([kri^ej for (ej, kri) in zip(ei, KRt.gens())])
H = Hom(KR, KRt)
h = F([P2])
"""


p_basis = PowerfulBasis(m, Kt)
d_basis = DecodingBasis(m, Kt)

#H = Hom(K, K)
#h = H([k^(-1)], check=False)


"""
Useless with iterators

# Complex embedding of K in C
C = ComplexField(53)
G = Hom(K, C)

# Without iterator!
omega = C(e^(2*I*pi/m))
zeta_m = [omega^i for i in range(m) if gcd(i, m) == 1]
sigma_i = [G([om], check=False) for om in zeta_m]
"""


# Step to construct Rcheck = < tinv >
g = prod([(1-ki) for ki in Kt.gens()])
#t = mhat / g
tinv = g / mhat

g2 = prod([(1-k^i) for i in coprime_list(m)])
tinv2 = g2 / mhat


# Dual lattices
Idual = Kt.ideal(tinv)
V = DFT(m).apply_map(conjugate)
tV = V.transpose()
B = DFT(m)
Bdual = ((tV*V*B).transpose())^(-1)


# CRT
#Iq = K.ideal(q)
#I_primes = Iq.prime_factors()
