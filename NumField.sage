# -*- coding: utf-8 -*-

#
# xgcd of a list a
#
def xgcd2(a):
    s = len(a)
    c = ZZ(int(random()*1000^s+1))
    L = matrix(ZZ, s+1, s, lambda i, j: kronecker_delta(i, j))
    L.set_row(s, a)
    L.rescale_row(s, c)
    U = (L.transpose()).LLL()
    u = list(U[s-1])[:-1]
    g = sum([ai*ui for (ai, ui) in zip(a, u)])

    return (g, u) if g !=0 else xgcd2(a)


# Parameters of RLWE instance
m = 3*5
n = euler_phi(m)
q = 31 # or q = 211 with m = 3*5*7 (to get q = 1 mod m)


# Factorisation of m
mi = [a^b for (a, b) in factor(m)]
mihat = [ZZ(m/mj) for mj in mi]
ei = xgcd2(mihat)[1]


# Fields
Q.<x> = QQ['x']
K.<k> = NumberField(cyclotomic_polynomial(m), check=False)
Idl = [cyclotomic_polynomial(mj) for mj in mi]
Ki = [NumberField(Idli, 'ki'+str(i)) for (i, Idli) in enumerate(Idl)]
Kt = NumberField(Idl, 'k', check=False)


# Construction of morphism f between K and Kt
P = prod([ki^ej for (ej, ki) in zip(ei, Kt.gens())])
F = Hom(K, Kt)
f = F([P])


# Basis of this fields
BK = Kt.integral_basis()
BR = K.integral_basis()


# Step to construct Rcheck = < tinv >
mhat = m/2 if m%2 == 0 else m
g = prod([(1-ki) for ki in Kt.gens()])
t = mhat / g
tinv = g / mhat


# Every morphism from K to CC
Sigma = K.embeddings(CC)


#
# Return a list with sigma_i(a) for i in Zm*
#
def sigma(a):
    return vector([si(a) for si in Sigma])


#
# omega_m
#
def omega(m):
    return e^(2*I*pi/m)


#
# Trace of an element
#
def Tr(a):
    # equivalent to reduce(lambda x, y: CC(x)+CC(y), sigma(a))
    return sum(sigma(a))

#
#  Norm of an element
#
def N(a):
    return prod(sigma(a))


#
# The absolute determinant of m
#
def absdet(m):
    return (m/prod([p^(1/(p-1)) for p in prime_factors(m)]))^euler_phi(m)


#
# Discrete Fourier Transform matrix
#
def DFT(m):
    return matrix(CC, m, m, lambda i, j: omega(m)^(i*j))


#
# Chinese Remainder Transform matrix
#
def CRT(m):
    D = DFT(m)
    C = D.matrix_from_rows_and_columns([i for i in xrange(m) 
                                        if gcd(i+1, m) == 1], 
                                       [j for j in xrange(euler_phi(m))])
    return C


#
# Return Zm* as a list
#
def coprime_list(m):
    return [k for k in range(m) if gcd(k, m) == 1]


#
# Useful to flatten tensor_product 
#
def to_list(l):
    return flatten(map(flatten, list(l)))


#
# Return p the powerful basis of K
#
def PowerfulBasis(m):
    l = [vector([ki^j for j in xrange(euler_phi(mj))]) 
         for (ki, mj) in zip(Kt.gens(), mi)]

    return vector(reduce(lambda x, y: 
                         to_list(vector(x).outer_product(vector(y))), l))



# Dual lattices
Idual = Kt.ideal(tinv)

V = DFT(m).apply_map(conjugate)

tV = V.transpose()

B = DFT(m)

Bdual = ((tV*V*B).transpose())^(-1)

#((V*Bdual).transpose()*V*B).apply_map(real_part).apply_map(round)


# CRT
Iq = K.ideal(q)

#I_primes = Iq.prime_factors()
