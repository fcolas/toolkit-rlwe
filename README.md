Attempt to implement [1] with sage.

How to use?

1) Launch sage in interpretor mode.

2) %attach "anyfile.sage"


NumField.sage : use NumberField() as main structure
Quotient.sage: use quotient() as main structure because NumberField() is too slow...


[1] A Toolkit for Ring-LWE Cryptography
http://eprint.iacr.org/2013/293
